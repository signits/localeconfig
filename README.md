localeconfig
=========

This module configures the locale and keyboard of the server

Requirements
------------

None

Role Variables
--------------

enable_localeconfig: true - [controls if the module will run at all]

keymap: "de-nodeadkeys" - [keymap to set]

locale: "en_US.utf8" - [locale to set]

tz: "Europe/Berlin" - [timezone to set]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
